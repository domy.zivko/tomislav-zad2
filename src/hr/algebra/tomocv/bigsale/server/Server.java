package hr.algebra.tomocv.bigsale.server;

import hr.algebra.tomocv.bigsale.model.Product;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Server implements ServerContract {
    private static final String FILE_PATH = "src/hr/algebra/tomocv/bigsale/Files/products.txt";

    @Override
    public Product getProductOnSale() throws IOException {
        String content = getData();
        return Product.createProduct(content);
    }

    private String getData() throws IOException {
        return new String(Files.readAllBytes(Paths.get(FILE_PATH)));
    }
}
