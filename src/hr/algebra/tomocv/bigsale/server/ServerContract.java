package hr.algebra.tomocv.bigsale.server;


import hr.algebra.tomocv.bigsale.model.Product;

import java.io.IOException;

public interface ServerContract {
    Product getProductOnSale() throws IOException;
}
