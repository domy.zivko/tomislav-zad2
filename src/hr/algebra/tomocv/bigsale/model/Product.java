package hr.algebra.tomocv.bigsale.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Product {
    private String name;
    private double price, discount, discountPrice;

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss a");

    private Product(String name, double price, double discount) {
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.discountPrice = price - price * discount;
    }

    public static Product createProduct(String content) {
        var data = content.split(";");
        return new Product(
                data[0],
                Double.parseDouble(data[1]),
                Double.parseDouble(data[2]) / 100d);
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getDiscount() {
        return discount;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    @Override
    public String toString() {
        String ls = System.lineSeparator();
        return "Name: " + getName() + ls +
                "Price: " + getPrice() + ls +
                "Discount: " + getDiscount() * 100 + "%" + ls +
                "Discount price: " + getDiscountPrice() + ls +
                "----------------------------------------" + ls;
    }

    public String toFileString() {
        String ls = System.lineSeparator();
        LocalDateTime localDateTime = LocalDateTime.now();
        String formattedDateTime = localDateTime.format(DATE_TIME_FORMATTER);
        return formattedDateTime + ";" + getName() + ";" + getDiscount() * 100 + "%" + ls;
    }
}
