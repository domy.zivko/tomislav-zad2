package hr.algebra.tomocv.bigsale;


import hr.algebra.tomocv.bigsale.server.Server;

public class Client {
    private static final int NUMBER_OF_REPETITIONS = 5;
    private static final long SLEEP_MILLIS = 5000;

    public static void main(String[] args) throws Exception {
        var server = new Server();
        var logger = new Logger();

        for (int i = 0; i < NUMBER_OF_REPETITIONS; i++) {
            var product = server.getProductOnSale();
            System.out.println(product.toString());
            logger.log(product.toFileString());
            Thread.sleep(SLEEP_MILLIS);
        }
    }
}
